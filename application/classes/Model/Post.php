<?php defined('SYSPATH') or die('No direct script access.');

class Model_Post extends ORM {
	protected $_table_name = 'kohana_posts';

	public function rules() {
		return array(
			'title' => array(
				array('not_empty')
			),
			'thumbnail' => array(
				array('not_empty')
			)
		);
	}
}