<?php defined('SYSPATH') or die('No direct script access.');

class Controller_FileUpload extends Controller {

	public function action_index()
	{
		$posts = ORM::factory('Post')->find_all();

		$view = View::factory('posts/index')
			->bind('posts', $posts);

		$this->response->body( $view );
	}

	/**
	 * Save New Post To Database
	 */
	public function action_save()
	{
		if($this->request->method() == Request::POST)
		{

			$saveFile = NULL;
			$title = $this->request->post('title');

			if (empty($title))
			{
				$this->response->body(
					json_encode(
						array('error' => 'Title is Required.')
					)
				);
				return FALSE;
			}

			if (isset($_FILES['thumbnail']) && !empty($_FILES['thumbnail']['name']))
            {
            	
                $path = $_FILES['thumbnail']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				$filename = basename($path, '.'.$ext).'_'.strtotime(date('YmdHis')).'.'.$ext;
				$pretty_filename = basename($path, '.'.$ext);
				$thumbnail = '/uploads/'.$filename;

				$saveFile = $this->_save_image($_FILES['thumbnail'], $filename);

                if ( ! $saveFile)
		        {
		            $error_message = 'There was a problem while uploading the image. Make sure it is uploaded and must be JPG/PNG/GIF file.';

		            $this->response->body(
						json_encode(
							array('error' => $error_message)
						)
					);

		            return FALSE;
		        }else{
		        	$post = ORM::factory('Post');
					$post->title = $title;
					$post->thumbnail = $thumbnail;
					$post->filename = $pretty_filename;// 'myfilename';
					$post->date_added = date(Date::$timestamp_format);
					$post->save();
					
					$this->response->body(
						json_encode(
							array(
								'id'			=> $post->id,
								'title'			=> $post->title,
								'thumbnail'		=> $post->thumbnail,
								'filename'		=> $post->filename,
								'date_added'	=> Date::formatted_time($post->date_added,'m-d-Y H:i:s')
							)
						)
					);
		        }

				
            }else{
            	$this->response->body(
					json_encode(
						array('error' => 'File is Required.')
					)
				);

	            return FALSE;
            }
		}else{
			$this->response->body(
				json_encode(
					array('error' => 'error.')
				)
			);

            return FALSE;			
		}
	}

	/**
	 * Saving of image
	 */
	protected function _save_image($image, $filename)
	{
		if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'public/uploads/';
 
        if ($file = Upload::save($image, NULL, $directory))
        {
 
            Image::factory($file)
                ->resize(60, 60, Image::AUTO)
                ->save($directory.$filename);
 
            // Delete the temporary file
            unlink($file);
 
            return $filename;
        }
 
        return FALSE;
	}

	/**
	 * Update Post
	 */
	public function action_update()
	{
		if($this->request->method() == Request::POST)
		{
			$id = (int) $this->request->post('upd_id');
			$title = $this->request->post('upd_title');
			$filename = $this->request->post('upd_filename');

			$post = ORM::factory('Post', $id);
			$post->title = $title;
			$post->filename = $filename;
			$post->save();

			$this->response->body(
				json_encode(
					array('success' => 'Updated Successfully.')
				)
			);

		}else{
			$this->response->body(
				json_encode(
					array('error' => 'error.')
				)
			);

            return FALSE;
		}
	}

	/**
	 * Delete Post
	 */
	public function action_delete()
	{
		if($this->request->method() == Request::POST)
		{
			$unique_id = (int) $this->request->post('id');

			$post = ORM::factory('Post', $unique_id);

			$post->delete();

			$this->response->body(
				json_encode(
					array('success' => 'Successfully  deleted.')
				)
			);

		}else{
			$this->response->body(
				json_encode(
					array('error' => 'error.')
				)
			);

            return FALSE;
		}
	}

} // End Controller
