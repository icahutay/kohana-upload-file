<html>
	<head>
		<title></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style type="text/css">
			body {font-family: Arial, Helvetica, sans-serif;}

			#newpostmodal, #updatepostmodal {
				display: none;
			}

			/* The Modal (background) */
			.modal {
			    display: none; /* Hidden by default */
			    position: fixed; /* Stay in place */
			    z-index: 1; /* Sit on top */
			    padding-top: 100px; /* Location of the box */
			    left: 0;
			    top: 0;
			    width: 100%; /* Full width */
			    height: 100%; /* Full height */
			    overflow: auto; /* Enable scroll if needed */
			    background-color: rgb(0,0,0); /* Fallback color */
			    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
			}

			/* Modal Content */
			.modal-content {
			    background-color: #fefefe;
			    margin: auto;
			    padding: 20px;
			    border: 1px solid #888;
			    width: 80%;
			}

			/* The Close Button */
			.close {
			    color: #aaaaaa;
			    float: right;
			    font-size: 28px;
			    font-weight: bold;
			}

			.close:hover,
			.close:focus {
			    color: #000;
			    text-decoration: none;
			    cursor: pointer;
			}

			input[type="text"] {
				width: 50%;
				padding: 5px;
			}

			input[type=button] {
				padding: 6px 10px;
			}

		</style>
	</head>
	<body>
		
		<h1>My Posts</h1>

		<button id="myBtn">Add New</button>
		<br><br>
		<table border="1" cellpadding="10" cellspacing="0">
			<thead>
				<tr>
					<th>Title</th>
					<th>Thumbnail</th>
					<th>Filename</th>
					<th>Date added</th>
					<th></th>
				</tr>
			</thead>

			<tbody id="tablebody">
				<?php foreach($posts as $post): ?>
					<tr id="tr_<?= $post->id; ?>">
						<td id="td_title_<?= $post->id; ?>"><?= $post->title; ?></td>
						<td>
							<img src="<?= $post->thumbnail; ?>" alt="">
						</td>
						<td id="td_filename_<?= $post->id; ?>"><?= $post->filename; ?></td>
						<td><?= Date::formatted_time($post->date_added,'m-d-Y H:i:s'); ?></td>
						<td>
							<button onClick="viewUpdatePostModal(<?= $post->id; ?>, '<?= $post->title; ?>', '<?= $post->filename; ?>')">Update</button>&nbsp;&nbsp;<button onClick="deletePost(<?= $post->id; ?>)">Delete</button>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<!-- The Modal -->
		<div id="myModal" class="modal">

		  <!-- Modal content -->
		  <div class="modal-content">
		    <span class="close">&times;</span>
		    <div id="newpostmodal">
		    	<h1>Add New Post</h1>

		    	<form id="frmnewpost" action="" method="post" enctype="multipart/form-data">
		    		<label for="title">Title</label>
		    		<input type="text" name="title">
		    		<br><br>

		    		<input type="file" name="thumbnail" id="thumbnail" />
		    		<br><br>

		    		<input type="button" value="Submit" onClick="saveNewPost()">
		    	</form>

		    </div>

		    <div id="updatepostmodal">
		    	<h1>Update Post</h1>

		    	<form id="frmupdatepost" action="" method="post" enctype="multipart/form-data">
		    		<input type="hidden" name="upd_id" value="">

		    		<label for="title">Title</label>
		    		<input type="text" name="upd_title">
		    		<br><br>

		    		<label for="title">FileName</label>
		    		<input type="text" name="upd_filename">
		    		<br><br>

		    		<input type="button" value="Update" onClick="updatePost()">
		    	</form>
		    </div>
		  </div>

		</div>

	</body>
</html>

<script src="/js/jquery.min.js"></script>

<script>
	// Get the modal
	var modal = document.getElementById('myModal');

	// Get the button that opens the modal
	var btn = document.getElementById("myBtn");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks the button, open the modal 
	btn.onclick = function() {
		//clear
		$('#frmnewpost input[name=title]').val('');
		$('#frmnewpost input[name=thumbnail]').val('');

		$('#updatepostmodal').hide();
		$('#newpostmodal').show();
	    modal.style.display = "block";

	}

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}

	function saveNewPost() {

		//validation
		if ($('#frmnewpost input[name=title]').val().trim() == '' ) {
			alert('Title is required.');
			return false;
		}


		if ($('#frmnewpost input[name=thumbnail]').val() == '') {
			alert('Please upload a file.');
			return false;	
		}

		var formData = new FormData($('#frmnewpost')[0]);

		$.ajax({
	        url: 'fileupload/save',
	        type: 'POST',
	        data: formData,
	        async: false,
	        cache: false,
	        contentType: false,
	        processData: false,
	        success: function (response) {
	        	var msg = JSON.parse(response);
	        	console.log(response);
	        	if (typeof msg.error !== 'undefined') {
	        		alert('Error\n\n'+msg.error);
	        		return false;
	        	}

	        	$newTr = "<tr id='tr_"+msg.id+"'>" +
	        				"<td id='td_title_"+msg.id+"'>"+msg.title+"</td>"+
	        				"<td><img src='"+msg.thumbnail+"' alt=''></td>"+
	        				"<td id='td_filename_"+msg.id+"'>"+msg.filename+"</td>"+
	        				"<td>"+msg.date_added+"</td>"+
	        				"<td>"+
	        					"<button onClick=\"viewUpdatePostModal("+msg.id+", '"+msg.title+"', '"+msg.filename+"')\">Update</button>&nbsp;&nbsp;"+
	        				"<button onClick='deletePost("+msg.id+")''>Delete</button></td>"+
	        			 "</tr>";

	        	$('#tablebody').append($newTr);
	        	modal.style.display = "none";

	        },
	        error: function(error) {
	        	console.log(error);
	        	alert('Error\n\nPlease check your entries........');
	        }
	    });
	}

	function viewUpdatePostModal(id, title, filename) {
		console.log(id, title, filename);

		$('#newpostmodal').hide();
		$('#updatepostmodal').show();


		$('input[name=upd_id]').val(id);
		$('input[name=upd_title]').val(title);
		$('input[name=upd_filename]').val(filename);

		modal.style.display = "block";
	}

	function updatePost()
	{
		console.log('updating post.......');

		var id_to_update = $('#frmupdatepost input[name=upd_id]').val();
		var title = $('#frmupdatepost input[name=upd_title]').val().trim();
		var filename = $('#frmupdatepost input[name=upd_filename]').val().trim();

		//validation
		if ($('#frmupdatepost input[name=upd_title]').val().trim() == '' ) {
			alert('Title is required.');
			return false;
		}


		if ($('#frmupdatepost input[name=upd_filename]').val().trim() == '' ) {
			alert('File Name is required.');
			return false;
		}

		var formData = $('#frmupdatepost').serialize();

		$.ajax({
	        url: 'fileupload/update',
	        type: 'POST',
	        data: formData,
	        success: function (response) {
	        	var msg = JSON.parse(response);
	        	console.log(response);
	        	if (typeof msg.error !== 'undefined') {
	        		alert('Error\n\n'+msg.error);
	        		return false;
	        	}

	        	console.log('id_to_update: '+id_to_update);
	        	console.log(title);

	        	$('#td_title_'+id_to_update).html(title);
	        	$('#td_filename_'+id_to_update).html(filename);

	        	modal.style.display = "none";

	        },
	        error: function(error) {
	        	console.log(error);
	        	alert('Error\n\nPlease check your entries........');
	        }
	    });
	}

	function deletePost(id) {
		console.log('deleting ....'+id);

		var confirm_delete = confirm("Are you sure you want to delete this post?");

		if (confirm_delete) {
			$.ajax({
		        url: 'fileupload/delete',
		        type: 'POST',
		        data: {'id' : id},
		        success: function (response) {
		        	var msg = JSON.parse(response);
		        	
		        	console.log(msg);

		        	if (typeof msg.error !== 'undefined') {
		        		alert('Error\n\n'+msg.error);
		        		return false;
		        	}

		        	if (msg.success) {
		        		$('#tr_'+id).remove();
		        	}
		        },
		        error: function(error) {
		        	console.log(error);
		        	alert('Error\n\nPlease check your entries........');
		        }
		    });
		}
	}
</script>